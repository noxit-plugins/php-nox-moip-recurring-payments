Moip { Assinaturas }
====================

O Moip Assinaturas (Pagamento Recorrente) é uma solução do Moip que permite você fazer cobranças de forma automática, no valor e intervalo que escolher por meio da criação de planos. Pelo Moip Assinaturas é possível gerenciar mensalidades, assinaturas e cobranças recorrentes, podendo ser utilizado desde para a cobrança de assinaturas de conteúdo até para a cobrança de utilização de softwares.

Esta é uma biblioteca PHP que se comunica com a API oficial do [Moip Assinaturas](http://dev.moip.com.br/assinaturas-api/).

Installation
------------
Prefira a instalação via [composer](http://getcomposer.org/download/).

Execute

```
php composer.phar require --prefer-dist nox-it/php-nox-moip-recurring-payments "*"
```

ou adicione

```
"nox-it/php-nox-moip-recurring-payments": "*"
```

à seção `require` do seu arquivo `composer.json`.
