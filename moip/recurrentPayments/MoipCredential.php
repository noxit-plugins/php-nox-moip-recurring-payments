<?php

    namespace nox\moip\recurrentPayments;

    /**
     * Class MoipCredential
     *
     * @package nox\moip\recurrentPayments
     */
    class MoipCredential extends Moip
    {
        /**
         * @var integer
         */
        public $environment = self::ENVIRONMENT_SANDBOX;

        /**
         * @var string
         */
        public $token = '';

        /**
         * @var string
         */
        public $apiKey = '';

        /**
         * Credentials constructor.
         *
         * @param string  $token
         * @param string  $apiKey
         * @param integer $type
         */
        public function __construct(string $token, string $apiKey, int $type)
        {
            if (!in_array($type, [self::ENVIRONMENT_PRODUCTION, self::ENVIRONMENT_SANDBOX])) {
                $this->environment = self::ENVIRONMENT_SANDBOX;
            }

            $this->token  = $token;
            $this->apiKey = $apiKey;
        }

        /**
         * @return string
         */
        public function getAuthorizationCode()
        {
            return base64_encode("{$this->token}:{$this->apiKey}");
        }
    }
