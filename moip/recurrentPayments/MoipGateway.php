<?php

    namespace nox\moip\recurrentPayments;

    /**
     * Class MoipGateway
     *
     * @package nox\moip\recurrentPayments
     */
    class MoipGateway extends Moip
    {
        /**
         * @var integer
         */
        public $environment = self::ENVIRONMENT_SANDBOX;

        /**
         * @var MoipCredential
         */
        protected $credential;

        /**
         * MoipGateway constructor.
         *
         * @param MoipCredential $credential
         */
        public function __construct(MoipCredential $credential)
        {
            $this->environment = $credential->environment;
            $this->credential = $credential;
        }
    }
